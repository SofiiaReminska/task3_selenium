package com.epam.lab;

import com.epam.lab.bo.ImportantMessageBO;
import com.epam.lab.bo.InboxBO;
import com.epam.lab.bo.LoginBO;
import com.epam.lab.bo.NavigateBO;
import com.epam.lab.utils.DriverManager;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class GmailTest {

    @Test
    public void loginMarkImportantAndDeleteMessage() {
        NavigateBO navigateBO = new NavigateBO();
        navigateBO.loadBasePage();
        LoginBO loginBO = new LoginBO();
        loginBO.login("sofii.test@gmail.com", "QwErTyUi");

        InboxBO inboxBO = new InboxBO();
        inboxBO.markMessagesAsImportant(3);
        inboxBO.navigateToImportantFolder();
        navigateBO.refreshPage();

        ImportantMessageBO importantMessageBO = new ImportantMessageBO();
        assertTrue(importantMessageBO.isMessagesMovedToImportant());
        importantMessageBO.deleteImportantMessages();
        assertTrue(importantMessageBO.isMessagesDeletedFromImportant());
    }

    @AfterMethod
    public void after() {
        DriverManager.getDriver().quit();
    }
}
