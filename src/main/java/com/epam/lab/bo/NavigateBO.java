package com.epam.lab.bo;

import com.epam.lab.utils.DriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class NavigateBO {
    private static final String BASE_URL = "https://mail.google.com";
    private static final Logger LOG = LogManager.getLogger(NavigateBO.class);

    public void loadBasePage() {
        LOG.info("Loading page {}", BASE_URL);
        DriverManager.getDriver().get(BASE_URL);
    }

    public void refreshPage() {
        LOG.info("Refreshing page");
        DriverManager.getDriver().navigate().refresh();
    }
}
