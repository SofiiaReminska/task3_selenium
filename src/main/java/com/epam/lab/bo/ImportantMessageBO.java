package com.epam.lab.bo;

import com.epam.lab.po.ImportantMessagePage;
import com.epam.lab.utils.DriverManager;
import com.epam.lab.utils.SavedContext;

import java.util.List;

public class ImportantMessageBO {

    private ImportantMessagePage importantMessagePage = new ImportantMessagePage(DriverManager.getDriver());

    @SuppressWarnings("unchecked")
    public boolean isMessagesMovedToImportant() {
        final List<String> expected = (List<String>) SavedContext.getSavedContext().get(SavedContext.SAVED_MESSAGE_IDS);
        return importantMessagePage.getMailIds().containsAll(expected);
    }

    public boolean isMessagesDeletedFromImportant() {
        return importantMessagePage.isNoImportantMessageLabelVisible();
    }

    public void deleteImportantMessages() {
        importantMessagePage.selectAllImportantMessages();
        importantMessagePage.deleteCheckedMessages();
    }
}
