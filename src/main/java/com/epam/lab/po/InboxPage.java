package com.epam.lab.po;

import com.epam.lab.Constants;
import com.epam.lab.utils.SavedContext;
import com.epam.lab.utils.WaitUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

import static com.epam.lab.utils.SavedContext.SAVED_MESSAGE_IDS;

public class InboxPage extends BasePage {
    private static final Logger LOG = LogManager.getLogger(InboxPage.class);

    @FindBy(xpath = "//tr[@jsaction]//*[@role='checkbox']/../following-sibling::td[1]/span")
    private List<WebElement> importantMessageCheckBoxes;

    @FindBy(xpath = "//a[contains(@href, '#starred')]")
    private WebElement importantMessagesFolder;

    public InboxPage(WebDriver driver) {
        super(driver);
    }

    public void markMessageAsImportant(int number) {
        final WebElement element = importantMessageCheckBoxes.get(number);
        saveMarkedMessageId(element);
        element.click();
    }

    public void clickOnImportantFolder() {
        WaitUtil.waitForVisibleAndClickable(importantMessagesFolder);
        importantMessagesFolder.click();
        WaitUtil.waitForInvisible(importantMessagesFolder);
    }

    @SuppressWarnings("unchecked")
    private void saveMarkedMessageId(WebElement element) {
        final List<String> list = (List<String>) SavedContext.getSavedContext().getOrDefault(SAVED_MESSAGE_IDS, new ArrayList<String>());
        list.add(element.findElement(Constants.MESSAGE_ID_XPATH).getAttribute(Constants.MESSAGE_ID_ATTRIBUTE));
        SavedContext.getSavedContext().put(SAVED_MESSAGE_IDS, list);
    }
}
