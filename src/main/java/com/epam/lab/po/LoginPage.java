package com.epam.lab.po;

import com.epam.lab.utils.DriverManager;
import com.epam.lab.utils.WaitUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.epam.lab.Constants.*;

public class LoginPage extends BasePage {
    private static final Logger LOG = LogManager.getLogger(LoginPage.class);

    @FindBy(xpath = "//*[@type='email']")
    private WebElement enterMailInput;

    @FindBy(id = "identifierNext")
    private WebElement identifierNextButton;

    @FindBy(xpath = "//*[@type='password']")
    private WebElement enterPasswordInput;

    @FindBy(id = "passwordNext")
    private WebElement passwordNextButton;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void enterEmail(String email) {
        LOG.info("Entering email...");
        enterMailInput.sendKeys(email);
    }

    public void enterPassword(String password) {
        LOG.info("Entering password...");
        enterPasswordInput.sendKeys(password);
    }

    public void clickIdentifierNextButton() {
        LOG.info("Clicking on next button...");
        identifierNextButton.click();
    }

    public void clickPasswordNextButton() {
        LOG.info("Clicking on next button...");
        passwordNextButton.click();
    }

    public void waitForEnterPassword() {
        LOG.info("Waiting for enter password");
        new WebDriverWait(DriverManager.getDriver(), DEFAULT_TIMEOUT)
                .until(ExpectedConditions.elementToBeClickable(enterPasswordInput));
    }

    public void waitForNextButtonToBeInvisible() {
        LOG.info("Waiting for next button to be invisible ");
        WaitUtil.waitForInvisible(passwordNextButton);
    }
}

