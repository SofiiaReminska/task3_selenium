package com.epam.lab.utils;

import java.util.HashMap;
import java.util.Map;

public class SavedContext {
    public static final String SAVED_MESSAGE_IDS = "savedMessageIds";
    private static Map<String, Object> savedContext = new HashMap<>();

    public static Map<String, Object> getSavedContext() {
        return savedContext;
    }

    @Override
    public String toString() {
        return savedContext.toString();
    }
}
