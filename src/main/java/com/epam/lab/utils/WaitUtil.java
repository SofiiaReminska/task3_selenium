package com.epam.lab.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.epam.lab.Constants.DEFAULT_TIMEOUT;

public class WaitUtil {
    private static final Logger LOG = LogManager.getLogger(WaitUtil.class);

    public static void waitForInvisible(WebElement element) {
        try {
            new WebDriverWait(DriverManager.getDriver(), DEFAULT_TIMEOUT).until(ExpectedConditions.invisibilityOf(element));
        } catch (TimeoutException | NoSuchElementException e) {
            LOG.warn(e);
        }
    }

    public static void waitForVisibleAndClickable(WebElement element) {
        new WebDriverWait(DriverManager.getDriver(), DEFAULT_TIMEOUT).until(ExpectedConditions.and(
                ExpectedConditions.visibilityOf(element), ExpectedConditions.elementToBeClickable(element)));
    }

    public static void waitConstTime() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            LOG.info(e);
        }
    }
}
